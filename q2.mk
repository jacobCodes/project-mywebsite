export PLATFORM ?= $(shell node -e "process.stdout.write(process.platform)")

export OPEN ?= open
ifeq ($(PLATFORM),win32)
	BANG ?= !
	MAKE ?= make
	NULL ?= nul
	SHELL := cmd.exe
else
	BANG ?= \!
	NULL ?= /dev/null
	SHELL := $(shell bash --version >$(NULL) 2>&1 && echo bash|| echo sh)
endif
ifeq ($(PLATFORM),darwin)
	export GREP ?= ggrep
	export SED ?= gsed
else
	export GREP ?= grep
	export SED ?= sed
endif
ifeq ($(PLATFORM), linux)
	export OPEN ?= xdg-open
endif

.EXPORT_ALL_VARIABLES:

export DOCKER ?= $(shell docker --version >$(NULL) 2>&1 && echo docker|| echo true)
export NOFAIL ?= 2>$(NULL)|| true
export DEVBOX ?= bash -c "$$(curl -fsSL https://q2sdk-utilities.s3.amazonaws.com/start_external_dev_machine.sh)"
export DIR_NAME ?= $(shell pwd | $(GREP) -Eo "[^\/]+$$")
export DEVBOX_NAME ?= $(DIR_NAME)_dev
export IS_DEVBOX ?= $(shell q2 --version >$(NULL) 2>&1 && echo true|| echo false)
export DEVBOX_EXISTS := $(shell ($(DOCKER) ps -a --format "table {{.Names}}" | \
	$(GREP) -E "^$(DEVBOX_NAME)$$" >$(NULL) 2>&1)&& echo "true"|| echo "false")
export DEVBOX_RUNNING := $(shell ($(DOCKER) ps --format "table {{.Names}}" | \
	$(GREP) -E "^$(DEVBOX_NAME)$$" >$(NULL) 2>&1)&& echo "true"|| echo "false")

ifeq ($(DEVBOX_EXISTS),true)
ifneq ($(wildcard .antilles/docker/env.sh),)
	export STACKNAME ?= $(shell mkdir -p .antilles/docker && \
		. .antilles/docker/env.sh && echo $$STACKNAME)
	export ANTILLES_SERVER_PORT ?= $(shell . .antilles/docker/env.sh && echo $$ANTILLES_SERVER_PORT)
	export CENTRAL_URL ?= https://stack.q2developer.com/sdk/$(STACKNAME)/hq/BackOffice.asmx
	export DATABASE_URL ?= sqlserver://$(SQL_SERVER_USER):$(SQL_SERVER_PWD)@$(SQL_SERVER_HOST):$(DEV_SERVER_DB_PORT)/$(SQL_SERVER_NAME)
	export DEVBOX_PIP ?= /code/.antilles/docker/.env/bin/pip
	export DEVBOX_PYTHON ?= /code/.antilles/docker/.env/bin/python
	export DEVBOX_Q2 ?= /code/.antilles/docker/.env/bin/q2
	export DEV_SERVER_DB_PORT ?= $(shell . .antilles/docker/env.sh && echo $$DEV_SERVER_DB_PORT)
  export DOCS_URL ?= https://docs.q2developer.com
	export ONLINE_URL ?= https://stack.q2developer.com/sdk/$(STACKNAME)/ardent/uux.aspx#/login
	export SDK ?= $(shell cat requirements.txt | $(GREP) q2-sdk | $(GREP) -oP '\d+\.\d+\.\d+')
	export SERVER_URL ?= https://sdk-shared-dev.q2devstack.com/$(ANTILLES_SERVER_PORT)
	export SQL_SERVER_HOST ?= $(shell . .antilles/docker/env.sh && echo $$SQL_SERVER_HOST)
	export SQL_SERVER_NAME ?= $(shell . .antilles/docker/env.sh && echo $$SQL_SERVER_NAME)
	export SQL_SERVER_PWD ?= $(shell . .antilles/docker/env.sh && echo $$SQL_SERVER_PWD)
	export SQL_SERVER_USER ?= $(shell . .antilles/docker/env.sh && echo $$SQL_SERVER_USER)
  export TECTON_URL ?= https://cdn4.onlineaccess1.com/cdn/base/tecton/latest/index.html
else
	export DEVBOX_EXISTS := false
	export DEVBOX_RUNNING := false
endif
endif

ifeq ($(OPEN_TERMINAL),)
	OPEN_TERMINAL := open-terminal
endif
ifeq ($(Q2_MAIN_EXTENSION),)
	OPEN_TERMINAL := open-terminal
endif

.PHONY: q2-devbox
q2-devbox:
ifeq ($(DEVBOX_EXISTS),true)
ifeq ($(DEVBOX_RUNNING),true)
	@docker exec -it $(DEVBOX_NAME) /bin/bash
else
	@docker start -ia $(DEVBOX_NAME)
endif
else
	@bash -c "$$(curl -fsSL https://q2sdk-utilities.s3.amazonaws.com/start_external_dev_machine.sh | \
		$(SED) -E 's/eval \$$DOCKER_RUN_CMD/# eval \$$DOCKER_RUN_CMD/g')" | tee .setup.log
ifeq ($(PLATFORM),linux)
	@$(MAKE) -s -f q2.mk q2-fix-permissions
endif
	@export DOCKER_RUN_CMD=$$(cat .setup.log | $(SED) -E 's/\x1b\[[0-9;]*m//g' | \
		$(GREP) -E "^Docker Command: docker run" | $(SED) -E 's/^Docker Command: //g') && \
		rm -f .setup.log && $$DOCKER_RUN_CMD
endif

.PHONY: q2-purge
q2-purge:
	-@$(DOCKER) run --rm -v $(shell pwd):/code busybox \
		/bin/sh -c "cd /code && chown -R \$$(stat -c '%u:%g' q2.mk) ."
	-@rm -rf .antilles
	-@$(DOCKER) rm -f "$(DEVBOX_NAME)" $(NOFAIL)

.PHONY: q2-devbox-start
q2-devbox-start: q2-devbox-exists
ifneq ($(DEVBOX_RUNNING),true)
	@$(OPEN_TERMINAL) '$(MAKE) -s devbox' &
	@echo waiting 10 seconds for devbox to start . . .
	@sleep 10
endif

.PHONY: q2-devbox-running
q2-devbox-running: q2-devbox-start q2-devbox-exists

.PHONY: q2-devbox-exists
q2-devbox-exists:
ifneq ($(DEVBOX_EXISTS),true)
	@echo "devbox does not exist! please run 'make devbox'" 1>&2
	@exit 1
endif

.PHONY: q2-devbox-stop
q2-devbox-stop:
	@docker stop $(DEVBOX_NAME) $(ARGS) $(NOFAIL)

.PHONY: q2-q2
q2-q2:
	@$(MAKE) -s q2-exec ARGS="$(DEVBOX_Q2) $(ARGS)"

.PHONY: q2-python
q2-python:
	@$(MAKE) -s q2-exec ARGS="$(DEVBOX_PYTHON) $(ARGS)"

.PHONY: q2-pip
q2-pip:
	@$(MAKE) -s q2-exec ARGS="$(DEVBOX_PIP) $(ARGS)"

.PHONY: q2-exec
q2-exec: q2-devbox-running
	@docker exec -it $(DEVBOX_NAME) /bin/bash -c "$(ARGS)"

.PHONY: q2-run
q2-run:
	@$(MAKE) -s -f q2.mk q2-q2 ARGS="run -a $(ARGS)"

FIX_PERMISSIONS_FILES := .
.PHONY: q2-fix-permissions
q2-fix-permissions:
ifeq ($(PLATFORM),linux)
ifeq ($(IS_DEVBOX),true)
	@cd /code && chown -R $$(stat -c '%u:%g' q2.mk) $(FIX_PERMISSIONS_FILES) && chown -R root:root /code/.antilles/docker/.ssh
else
	@$(DOCKER) run --rm -v $(shell pwd):/code busybox \
		/bin/sh -c "cd /code && chown -R \$$(stat -c '%u:%g' q2.mk) $(FIX_PERMISSIONS_FILES) && chown -R root:root /code/.antilles/docker/.ssh"
endif
endif

.PHONY: q2-reset
q2-reset:
	@$(MAKE) -s -f q2.mk q2-purge
	@$(MAKE) -s -f q2.mk q2-devbox

.PHONY: q2-open q2-open-%
q2-open: q2-open-server
q2-open-server: q2-devbox-exists
	@export URL=$(SERVER_URL) && \
		echo opening $$URL && $(OPEN) $$URL
q2-open-online: q2-devbox-exists
	@export URL=$(ONLINE_URL) && \
		echo opening $$URL && $(OPEN) $$URL
q2-open-central: q2-devbox-exists
	@export URL=$(CENTRAL_URL) && \
		echo opening $$URL && $(OPEN) $$URL
q2-open-%: q2-devbox-exists
	@export URL=$(ONLINE_URL)#/extension/$(shell echo $@ | $(SED) -E "s/^q2-open-//g")/Main && \
		echo opening $$URL && $(OPEN) $$URL
q2-open-docs:
	@export URL=$(DOCS_URL) && \
		echo opening $$URL && $(OPEN) $$URL
q2-open-tecton:
	@export URL=$(TECTON_URL) && \
		echo opening $$URL && $(OPEN) $$URL

.PHONY: q2-get-env
q2-get-envs: q2-devbox-exists
	@cat .antilles/docker/env.sh

.PHONY: info
q2-info: q2-devbox-exists
	@echo "Stack Name:      $(STACKNAME)"
	@echo "SDK Version:     $(SDK)"
	@echo "SDK Server Port: $(ANTILLES_SERVER_PORT)"
	@echo "SDK Server URL:  $(SERVER_URL)"
	@echo "Database:        $(DATABASE_URL)"
	@echo "Online URL:      $(ONLINE_URL)"
	@echo "Central URL:     $(CENTRAL_URL)"
	@echo "Forum:           http://help.q2developer.com"
	@echo "SDK Docs         https://docs.q2developer.com"
	@echo "Tecton Docs      https://cdn4.onlineaccess1.com/cdn/base/tecton/latest/index.html$(NC)"
