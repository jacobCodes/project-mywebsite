"""
ExampleExternalAccount Extension
"""
import datetime
from injector import Injector, CallableProvider, noscope
from q2_sdk.core.http_handlers.tecton_client_handler import Q2TectonClientRequestHandler
from .install.db_plan import DbPlan
from .routes import Routes
from bindings import Context, Logger
from request_scope import RequestScope



class ExampleExternalAccountHandler(Q2TectonClientRequestHandler):

    CONFIG_FILE_NAME = 'ExampleExternalAccount' # configuration/ExampleExternalAccount.py file must exist if REQUIRED_CONFIGURATIONS exist
    DB_PLAN = DbPlan()
    # REQUIRED_CONFIGURATIONS = {'FEATURE': None}

    FRIENDLY_NAME = 'ExampleExternalAccount' # this will be used for end user facing references to this extension like Central and Menu items.
    DEFAULT_MENU_ICON = 'landing-page' # this will be the default icon used if extension placed at top level (not used if a child element)
    injector = Injector()

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.request_scope = None
        self._services = None
        self._procedures = None
        self.allow_non_q2_traffic = True



    @property
    def router(self):
        router = super().router
        self.update_bindings()
        routes = self.injector.get(Routes)
        for route_name in dir(routes):
            if route_name[0] != '_' and route_name[0].islower() and callable(
                getattr(routes, route_name)
            ):
                router.update({route_name: getattr(routes, route_name)})
        return router

    async def default(self):
        result = await request_extension_data({"route":"get_balance_info","body":{}},"ExternalAccount")
        print(result,"result")
        return {'message': 'Hello World from extension'}

    def prepare(self):
        if self.request.headers.get(
            'cookie'
        ) == 'Q2API-Compatibility=universal':
            self.request_scope = self.injector.get(RequestScope)
            # self.request_scope.enter()
        super().prepare()

    def on_finish(self):
        if self.request_scope:
            self.request_scope.exit()
        super().on_finish()

    def update_bindings(self):
        self.injector.binder.bind(
            Context, to=CallableProvider(self.get_context), scope=noscope
        )
        self.injector.binder.bind(
            Logger, to=CallableProvider(self.get_logger), scope=noscope
        )

    def get_context(self):
        return self

    def get_logger(self):
        return self.logger
    