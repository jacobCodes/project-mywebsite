import React, { FC, useEffect, useState } from 'react';
import { Q2Btn } from 'q2-tecton-react';
import { useActions, useSources } from 'q2-tecton-hooks';
import { IActions, ISources } from 'q2-tecton-sdk';

export interface HomeProps {}

const Home: FC<HomeProps> = () => {
  const actions: IActions | null = useActions();
  const sources: ISources | null = useSources();
  const [details, setDetails] = useState({});
  const [balanceInfo, setBalanceInfo] = useState(false);

  useEffect(() => {
    console.log('use effect12');
    sources?.paramsChanged((newParams: any) => {
      setDetails(newParams?.bankDetails);
      console.log('newparams', newParams);
      // actions?.clearParams!();
    });
  }, [actions]);

  async function getRequestData() {
    // const data = await sources?.requestPlatformData!({
    //   route: 'get_request_data',
    //   method: 'POST'
    //   // body: JSON.stringify({ name: 'Tecton', email: 'tecton@q2.com' })
    // });

    const data = await sources?.requestExtensionData!({
      route: 'get_request_data'
      // body: {
      //   url: 'https://sdk-shared-dev.q2devstack.com/2419/ExternalAccount'
      // }
    });
    console.log(data, 'data');

    console.log('hello world');
  }

  function getAccountDetails() {
    actions?.navigateTo!('ExternalAccount', 'Main', {
      requestType: 'identityInfo'
    });
  }
  function getBalanceInfo() {
    actions?.navigateTo!('ExternalAccount', 'Main', {
      requestType: 'balanceInfo'
    });
    setBalanceInfo(true);
    // actions?.navigateTo!('ExternalAccount', 'Main');
  }

  function renderData() {
    return (
      <div>
        {!balanceInfo ? (
          details.data?.data[0].addresses.map((value) => {
            return (
              <div>
                <ol>
                  <li>city:{value.data.city}</li>
                  <li>country:{value.data.country}</li>
                  {/* <li>
                    balance:{value.balances?.current}
                    {value.balances?.iso_currency_code}
                  </li> */}
                </ol>
              </div>
            );
          })
        ) : (
          <div>
            {details.data?.data.map((value) => {
              return (
                <div>
                  <ol>
                    <li>account name:{value.name}</li>
                    <li>account_offical_name:{value.official_name}</li>
                    <li>
                      balance:{value.balances?.current}
                      {value.balances?.iso_currency_code}
                    </li>
                  </ol>
                </div>
              );
            })}
            )
          </div>
        )}
      </div>
    );
  }

  return (
    <div>
      {details?.data ? (
        renderData()
      ) : (
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around'
          }}
        >
          <Q2Btn color="primary" onClick={() => getRequestData()}>
            get_request_data
          </Q2Btn>
          <Q2Btn color="primary" onClick={() => getBalanceInfo()}>
            Get BalanceInfo
          </Q2Btn>
          <Q2Btn color="primary" onClick={() => getAccountDetails()}>
            Get IdentityInfo
          </Q2Btn>
        </div>
      )}
    </div>
  );
};

export default Home;
