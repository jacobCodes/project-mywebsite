import '@storybook/addon-console';
import { addDecorator, addParameters } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';
import { withTests } from '@storybook/addon-jest';
import themes from './themes';
// @ts-ignore
// eslint-disable-next-line import/no-unresolved
import results from '../node_modules/.tmp/jestTestResults.json';
import withTecton from './withTecton';

addDecorator(withA11y as any);
addDecorator(withTests({ results }));
addDecorator(withTecton);
addParameters({
  themeUi: { themes }
});
