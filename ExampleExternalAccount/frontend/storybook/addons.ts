import { addons } from '@storybook/addons';
import { connect } from 'q2-tecton-sdk';

addons.register('q2/tecton', async () => {
  const tecton = await connect();
  if (typeof tecton === 'undefined') throw new Error('tecton failed to load');
  try {
    tecton.actions?.setFetching!(false);
  } catch (err) {
    tecton.actions?.showModal!({
      title: 'Error',
      message: err.data.message,
      modalType: 'error',
      close: false
    });
  }
});
