from injector import inject
from bindings import Context
from services import Q2
from munch import munchify


@inject
class Routes:
    def __init__(self, context: Context,q2:Q2):
        self.context = context
        self.q2 = q2
        
    async def default(self):
        print("hello")
        # print(result,"result")
        return {
            'context_id': id(self.context.request),
        }
        
    async def get(self):
        return {"data":"hello world"}    


    async def get_request_data(self):
        data = munchify(self.context.form_fields)
        # url=data['url']
        try:
            url = "https://sdk-shared-dev.q2devstack.com/2419/TestExternalAccount"
            request_options = {"route":"test_external","body":{}}
            res = await self.q2.get_request_extension_data(request_options,url=url)
            # res = await self.q2.get_request_extension_data(request_options)
            print('route response')
            # print(str(dir(res)),"res")
            print(res["error"],"res")
            print('hello dold')
            return {"hello world": 'world'}
        except Exception as e:
            print(str(e)+"error")
            return str(e)
