from q2_sdk.hq.hq_api.wedge_online_banking import ExecuteWedgeRequestByName
from q2_sdk.core import q2_requests

async def request_extension_data(request_options,extension=None,url=None):
    if url:
        # request url (method 2)
        response = q2_requests.post(self.logger,"https://sdk-shared-dev.q2devstack.com/2419/ExternalAccount",params = {"formData":"routing_key=get_link_token&data=e30%3D"})
        return {"data":response}
    # resquest extension code (method 1)
    params_obj = ExecuteWedgeRequestByName.ParamsObj(logger=self.logger,hq_credentials=self.context.hq_credentials,wedge_address_short_name="get_access_token",wedge_request_data=None)
    response = await ExecuteWedgeRequestByName.execute(params_obj,use_json=False)
    return {"data":response}
    
    