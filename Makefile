export MAKE_CACHE := $(shell pwd)/.env/.make
export PARENT := true
include blackmagic.mk
export Q2_READY := $(MAKE_CACHE)/q2_ready

export CLOC ?= cloc
export COVERAGE ?= /code/.antilles/docker/.env/bin/coverage
export CSPELL ?= cspell
export PYLINT ?= .env/bin/pylint
export UNIFY ?= .env/bin/unify
export VIRTUALENV ?= virtualenv
export YAPF ?= .env/bin/yapf

include q2.mk

export GLOBAL_FILES := $(shell $(GIT) ls-files 2>$(NULL) | $(SED) "s/.*\/frontend\/.*//g" | $(GREP) -v -E "^$$")

ACTIONS += install
INSTALL_DEPS := $(call deps,install,requirements-dev.txt requirements.txt \
	$(shell $(GIT) ls-files 2>$(NULL) | $(GREP) -E "package.json"))
INSTALL_TARGET := .env/bin/pip
$(ACTION)/install:
	@$(VIRTUALENV) .env
	@$(PIP) install -r requirements-dev.txt
	@$(MAKE) -e -s .env/downloads/done
	@$(MAKE) -s pip ARGS="install -r requirements-dev.txt"
	@cat requirements.txt | $(SED) 's|q2-sdk==.*||g' | $(SED) 's|#.*||g' | xargs $(PIP) install
	@$(PIP) install .env/downloads/$$(ls .env/downloads | $(GREP) -E "^q2_sdk-.*\.whl$$")
	@$(call run_child_command,install)
	@$(call done,install)
.env/downloads/done: requirements.txt
	@mkdir -p .env/downloads
	@$(MAKE) -s pip ARGS="download q2-sdk -d .env/downloads"
	@$(MAKE) -s fix-permissions
	@touch -m .env/downloads/done
.env/bin/pip: ;

ACTIONS += upgrade~install
$(ACTION)/upgrade:
	# @$(MAKE) -s q2 ARGS="upgrade"
	# @$(MAKE) -s fix-permissions
	@$(call run_child_command,upgrade)
	@$(call done,upgrade)

ACTIONS += format~install
FORMAT_DEPS := $(call deps,format,$(shell $(GIT) ls-files 2>$(NULL) | \
	$(GREP) -E "\.((py)|(json)|(ya?ml)|(md)|([jt]sx?))$$"))
$(ACTION)/format:
	-@$(UNIFY) -i -r $(shell echo $(GLOBAL_FILES) | $(SED) "s/ /\n/g" | $(GREP) -E "\.py$$")
	-@$(YAPF) -vv -ipr $(shell echo $(GLOBAL_FILES) | $(SED) "s/ /\n/g" | $(GREP) -E "\.py$$")
	-@$(call run_child_command,format)
	@$(call done,format)

ACTIONS += spellcheck~format
SPELLCHECK_DEPS := $(call deps,spellcheck,$(shell $(GIT) ls-files 2>$(NULL) | \
	$(GREP) -E "\.((json)|(ya?ml)|(md)|([jt]sx?))$$"))
$(ACTION)/spellcheck:
	-@$(CSPELL) --config .cspellrc.json $(shell echo $(GLOBAL_FILES) | $(SED) 's/ /\n/g' | \
		$(GREP) -E "\.((py)|(json)|(ya?ml)|(md)|([jt]sx?))$$")
	-@$(call run_child_command,spellcheck)
	@$(call done,spellcheck)

ACTIONS += lint~spellcheck
LINT_DEPS := $(call deps,lint,$(shell $(GIT) ls-files 2>$(NULL) | \
	grep -E "\.((py)|([jt]sx?))$$"))
$(ACTION)/lint:
	-@mkdir -p .env/.tmp
	-@$(PYLINT) -r n -f colorized configuration $(Q2_EXTENSIONS) - \
		msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" | tee .env/.tmp/pylint.txt || true
	-@$(call run_child_command,lint)
	@$(call done,lint)

ACTIONS += test~lint
TEST_DEPS := $(call deps,test,$(shell $(GIT) ls-files 2>$(NULL) | \
	$(GREP) -E "\.((py)|([jt]sx?))$$"))
$(ACTION)/test:
	-@$(MAKE) -s exec ARGS="$(COVERAGE) erase"
	-@$(MAKE) -s exec \
		ARGS='$(COVERAGE) run --omit=".env/*" --branch --source="." -m pytest -W ignore::DeprecationWarning --junitxml=report.xml --ignore-glob=env --ignore-glob=venv --ignore-glob="**/node_modules" --ignore-glob="**/.cache" --ignore-glob="**/dist"'
	-@$(MAKE) -s exec ARGS="$(COVERAGE) xml -o coverage.xml -i"
	-@$(MAKE) -s exec ARGS='sed -i "s|$$(pwd)|.|g" coverage.xml'
	-@$(MAKE) -s exec ARGS="$(COVERAGE) report -m"
	-@$(call run_child_command,test)
	@$(call done,test)

ACTIONS += build~test
BUILD_DEPS := $(call deps,build,$(shell $(GIT) ls-files 2>$(NULL) | \
	$(GREP) -E "\.((py)|([jt]sx?))$$"))
$(ACTION)/build:
	@$(call run_child_command,build)
	@$(call done,build)

.PHONY: clean +clean
clean: +clean
+clean: fix-permissions
	-@$(call clean)
	-@$(call run_command,+clean)
	-@$(GIT) clean -fXd \
		-e $(BANG)/**/node_modules \
		-e $(BANG)/**/node_modules/**/* \
		-e $(BANG)/**/package-lock.json \
		-e $(BANG)/**/pnpm-lock.yaml \
		-e $(BANG)/**/yarn.lock \
		-e $(BANG)/.antilles \
		-e $(BANG)/.antilles/**/* \
		-e $(BANG)/.env \
		-e $(BANG)/.env/**/* \
		-e $(BANG)/.vscode \
		-e $(BANG)/.vscode/**/* $(NOFAIL)
	-@rm -rf .env/.make $(NOFAIL)

.PHONY: purge +purge
purge: +purge
+purge: clean q2-purge
	-@$(call run_command,+purge)
ifeq ($(IS_DEVBOX), true)
	-@$(GIT) clean -fXd \
		-e $(BANG)/.env \
		-e $(BANG)/.env/**/* $(NOFAIL)
else
	-@$(GIT) clean -fXd $(NOFAIL)
endif
	-@$(GIT) prune $(NOFAIL)

.PHONY: count
count:
	@$(CLOC) $(shell $(GIT) ls-files)

.PHONY: devbox-start devbox-stop devbox exec fix-permissions get-envs info \
	open-central open-online open pip python q2 open-%
devbox-start: q2-devbox-start
devbox-stop: q2-devbox-stop
devbox: q2-devbox
exec: q2-exec
fix-permissions: q2-fix-permissions
get-envs: q2-get-envs
info: q2-info
open: q2-open
pip: q2-pip
python: q2-python
q2: q2-q2
open-%:
	@$(MAKE) -e -s q2-$@

-include $(patsubst %,$(_ACTIONS)/%,$(ACTIONS))

+%:
	@$(MAKE) -e -s $(shell echo $@ | $(SED) 's/^\+//g')

%: ;

CACHE_ENVS += \
	PIP \
	PYTHON \
	CLOC \
	CSPELL \
	PYLINT \
	UNIFY \
	YAPF

ifeq ($(DEVBOX_EXISTS),true)
	export CACHE_ENVS += \
		DOCKER \
		DEVBOX \
		DIR_NAME \
		DEVBOX_NAME \
		STACKNAME \
		ANTILLES_SERVER_PORT \
		CENTRAL_URL \
		DATABASE_URL \
		DEVBOX_PIP \
		DEVBOX_PYTHON \
		DEVBOX_Q2 \
		DEV_SERVER_DB_PORT \
		ONLINE_URL \
		SDK \
		SERVER_URL \
		SQL_SERVER_HOST \
		SQL_SERVER_NAME \
		SQL_SERVER_PWD \
		SQL_SERVER_USER
ifeq ($(wildcard $(Q2_READY)),)
  _RUN := $(shell rm -rf $(ENVS) $(NOFAIL) && \
    touch $(Q2_READY))
endif
endif
