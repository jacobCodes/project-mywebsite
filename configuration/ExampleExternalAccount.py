##### NON EXPORTED CODE HERE -- PREPEND ALL NON EXPORTED VARIABLES WITH __ #####

import sys

if '-a' in sys.argv:
    __url = 'http://localhost:3001'
else:
    __url = '{BASE_URL}/index.html'

##### DO NOT EDIT BELOW THIS LINE -- AUTO GENERATED CONFIG #####

FEATURE = {
    "core": False,
    "modules": {
        "Main": {
            "overpanel": False,
            "primary": True,
            "navigable": True,
            "formPostAuth": False,
             'url': '{0}'.format(__url),
            "meta": {
                "type": {
                    "shape": "Content",
                    "context": "None",
                    "overpanelReady": False,
                },
                'params': {
                    'bankDetails': {
                        'replace': True,
                        'type': 'string'
                    }
                }
            }
        }
    }
}
