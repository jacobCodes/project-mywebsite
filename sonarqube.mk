ifeq ($(PLATFORM),win32)
	BANG ?= !
	MAKE ?= make
	NULL ?= nul
	SHELL := cmd.exe
else
	BANG ?= \!
	NULL ?= /dev/null
	SHELL := $(shell bash --version >$(NULL) 2>&1 && echo bash|| echo sh)
endif
ifeq ($(PLATFORM),darwin)
	export GREP ?= ggrep
	export SED ?= gsed
else
	export GREP ?= grep
	export SED ?= sed
endif

ifeq ($(Q2_EXTENSIONS),)
	Q2_EXTENSIONS := $(shell python -c "$$(echo 'print(" ".join(INSTALLED_EXTENSIONS))' | cat configuration/settings.py - | $(SED) 's/from q2_sdk/\# from q2_sdk/g')")
endif
ifeq ($(NODE_PACKAGES),)
	NODE_PACKAGES := $(shell ls packages)
endif
Q2_MAIN_EXTENSION := $(shell echo $(Q2_EXTENSIONS) | cut -d " " -f1)

RC_GITLAB_SONARQUBE_ECHO_COVERAGE := $(Q2_MAIN_EXTENSION)/frontend/node_modules/.bin/rc-gitlab-sonarqube-echo-coverage

# Config
SONAR_QUALITYGATE_WAIT := true

# Files
SONAR_TEST_INCLUSIONS := \
	\*/tests/\*\*/\*.py \
	\*\*/src/\*\*/\*.test.tsx \
	\*\*/src/\*\*/\*.test.ts \
	\*\*/src/\*\*/\*.spec.tsx \
	\*\*/src/\*\*/\*.spec.ts \
	\*\*/src/\*\*/\spec.ts \
	\*\*/src/\*\*/\test.ts \
	\*\*/tests/\*\*/\*.ts \
	\*\*/tests/\*\*/\*.tsx
SONAR_EXCLUSIONS := \
	\*\*/\*.d.ts \
	\*\*/\*.d.tsx \
	\*\*/\*.js \
	\*\*/\*.story.ts \
	\*\*/\*.story.tsx \
	\*\*/src/@types/\*\*/\* \
	\*\*/storybook/\*\*/\* \
	\*/install/\*\*/\* \
	\*/urls.py \
	configuration/\*\*/\*
SONAR_DUPLICATION_EXCLUSIONS := \
  coverage.xml \
  report.xml

# Linting
SONAR_ESLINT_REPORT_PATHS := \
	$(patsubst %,%/frontend/node_modules/.tmp/eslintReport.json,$(Q2_EXTENSIONS)) \
	$(patsubst %,packages/%/node_modules/.tmp/eslintReport.json,$(NODE_PACKAGES))
SONAR_PYTHON_XUNIT_REPORT_PATH := report.xml
SONAR_PYTHON_PYLINT_REPORT_PATH := .env/.tmp/pylint.txt

# Coverage
SONAR_TEST_EXECUTION_REPORT_PATHS := \
	$(patsubst %,%/frontend/node_modules/.tmp/reports/test-report.xml,$(Q2_EXTENSIONS)) \
	$(patsubst %,packages/%/node_modules/.tmp/reports/test-report.xml,$(NODE_PACKAGES))
SONAR_TYPESCRIPT_LCOV_REPORT_PATHS := \
	$(patsubst %,%/frontend/node_modules/.tmp/coverage/lcov.info,$(Q2_EXTENSIONS)) \
	$(patsubst %,packages/%/node_modules/.tmp/coverage/lcov.info,$(NODE_PACKAGES))
SONAR_PYTHON_COVERAGE_REPORT_PATHS := coverage.xml

.PHONY: sonarqube
sonarqube: test
	@mkdir -p .env/.tmp
	@sonar-scanner \
		-Dsonar.eslint.reportPaths='$(shell echo $(SONAR_ESLINT_REPORT_PATHS) | $(SED) 's/ /,/g')' \
		-Dsonar.exclusions='$(shell echo $(SONAR_EXCLUSIONS) | $(SED) 's/ /,/g')' \
		-Dsonar.host.url='$(SONAR_HOST_URL)' \
		-Dsonar.projectKey='$(CI_PROJECT_NAME)' \
		-Dsonar.python.coverage.reportPaths='$(shell echo $(SONAR_PYTHON_COVERAGE_REPORT_PATHS) | $(SED) 's/ /,/g')' \
		-Dsonar.python.pylint.reportPath='$(SONAR_PYTHON_PYLINT_REPORT_PATH)' \
		-Dsonar.python.xunit.reportPath='$(SONAR_PYTHON_XUNIT_REPORT_PATH)' \
		-Dsonar.qualitygate.wait=$(SONAR_QUALITYGATE_WAIT) \
		-Dsonar.test.inclusions='$(shell echo $(SONAR_TEST_INCLUSIONS) | $(SED) 's/ /,/g')' \
		-Dsonar.testExecutionReportPaths='$(shell echo $(SONAR_TEST_EXECUTION_REPORT_PATHS) | $(SED) 's/ /,/g')' \
		-Dsonar.token='$(SONAR_TOKEN)' \
		-Dsonar.typescript.lcov.reportPaths='$(shell echo $(SONAR_TYPESCRIPT_LCOV_REPORT_PATHS) | $(SED) 's/ /,/g')' \
	  -Dsonar.cpd.exclusions='$(shell echo $(SONAR_DUPLICATION_EXCLUSIONS) | $(SED) 's/ /,/g')' \
    2>&1 | tee .env/.tmp/stdout.log
	@cat .env/.tmp/stdout.log | $(GREP) -q "QUALITY GATE STATUS:"

.PHONY: sonarqube-coverage
sonarqube-coverage:
	@SONAR_HOST=$(SONAR_HOST_URL) SONAR_PROJECT=$(CI_PROJECT_NAME) $(RC_GITLAB_SONARQUBE_ECHO_COVERAGE)
