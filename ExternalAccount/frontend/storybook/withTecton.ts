import { connect } from 'q2-tecton-sdk';
import { makeDecorator } from '@storybook/addons';

async function tectonConnect() {
  const tecton = await connect();
  if (typeof tecton === 'undefined') throw new Error('tecton failed to load');
  try {
    tecton.actions?.setFetching!(false);
  } catch (err) {
    tecton.actions?.showModal!({
      title: 'Error',
      message: err.data.message,
      modalType: 'error',
      close: false
    });
  }
}

export default makeDecorator({
  name: 'withTecton',
  parameterName: 'tecton',
  wrapper: (storyFn, context) => {
    tectonConnect();
    return storyFn(context);
  }
});
