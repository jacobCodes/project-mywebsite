import { ReducersMapObject } from 'redux';
import { State } from '~/store';
import { linkToken } from '~/modules/link/reducer';
import { accessToken as data } from '~/modules/accessToken/reducer';

export default { linkToken, data } as ReducersMapObject<State>;
