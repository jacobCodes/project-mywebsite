import React, { useCallback, useState, useEffect } from 'react';
import { usePlaidLink } from 'react-plaid-link';
import { IActions } from 'q2-tecton-sdk';
import { Q2Btn, Q2Loading } from 'q2-tecton-react';
import { useSources, useActions } from 'q2-tecton-hooks';
import { useAccessToken } from '~/modules/accessToken/hooks';

const Link = (props) => {
  const sources = useSources();
  const actions: IActions | null = useActions();
  const [accessToken, fetchAccessToken] = useAccessToken();
  console.log('accessToken', accessToken);
  const [verified, setVerified] = useState(false);

  const onSuccess = useCallback(async (token, metadata) => {
    setVerified(true);
    // send token to server
    // await fetchAccessToken(metadata);
    // await handleGetParams();
    // await actions?.navigateTo!('ExampleEXternalAccount', 'Main', {
    //   bankDetails: accessToken
    // });
    const data = await sources?.requestExtensionData!({
      route: 'get_access_token'
    });
    console.log('data', data);
  }, []);

  const config = {
    token: props.token,
    onSuccess
    // ...
  };

  useEffect(() => {
    if (accessToken !== undefined) {
      props.getDetails(accessToken);
    }
  }, [accessToken]);

  // async function handleGetParams() {
  //   console.log(accessToken, 'access');
  // }

  const { open, ready, error } = usePlaidLink(config);

  function renderData() {
    return (
      <div>
        {accessToken.accountDetails ? (
          accessToken.accountDetails.numbers.ach.map((value) => {
            return (
              <div>
                <ol>
                  <li>account number:{value.account}</li>
                  <li>account_id:{value.account_id}</li>
                  <li>routing number:{value.routing}</li>
                </ol>
              </div>
            );
          })
        ) : (
          <Q2Loading />
        )}
      </div>
    );
  }

  function renderDetails() {
    return (
      <div>
        {verified ? (
          <div>{renderData()}</div>
        ) : (
          <div>
            <Q2Btn onClick={() => open()} color="primary">
              Connect a bank account
            </Q2Btn>
          </div>
        )}
      </div>
    );
  }
  return <div>{renderDetails()}</div>;
};
export default Link;
