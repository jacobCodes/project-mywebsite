import React, { FC, useState, useCallback, useEffect } from 'react';
import { usePlaidLink } from 'react-plaid-link';

import { Q2Btn, Q2Loading } from 'q2-tecton-react';
import { useSources, useActions } from 'q2-tecton-hooks';
import Link from '../components/link';
import { useLinkToken } from '~/modules/link/hooks';

export interface HomeProps {}

const Home: FC<HomeProps> = () => {
  const [linkToken, fetchLinkToken] = useLinkToken();
  const sources = useSources();
  const actions = useActions();
  const [ifToken, setIfToken] = useState(true);
  const [details, setDetails] = useState({});
  const [paramsChanged, setParamsChanged] = useState('');

  useEffect(() => {
    console.log('use effect34');
    fetchValue();

    sources?.paramsChanged(async (newParams: any) => {
      console.log(newParams?.userInput, 'new params');
      setParamsChanged(newParams?.userInput);
      if (ifToken === true) {
        console.log('else');
        fetchLinkToken();
      } else {
        console.log('hello');
        if (newParams?.userInput === 'balanceInfo') {
          console.log('newParams', newParams);
          const data: any = await sources?.requestExtensionData!({
            route: 'get_balance_info'
          });
          console.log('data', data);
          setDetails(data);
        }
        if (newParams?.userInput === 'identityInfo') {
          console.log('newParams', newParams);
          const data: any = await sources?.requestExtensionData!({
            route: 'get_identity_data'
          });
          setDetails(data);
        }
      }
    });
  }, [sources, actions, ifToken]);

  async function fetchValue() {
    const value = await sources?.requestExtensionData({
      route: 'user_validation'
    });
    console.log('value', value?.data?.acces_token);
    setIfToken(value?.data?.acces_token);
  }

  useEffect(() => {
    console.log('details', details);
    if (details?.data) {
      actions?.navigateTo!('ExampleExternalAccount', 'Main', {
        bankDetails: details
      });
    }
  }, [details]);

  // function renderGetData() {
  //   return <div>{!ifToken ? <Q2Loading /> : <div>hello</div>}</div>;
  // }

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      {!linkToken ? (
        <Link
          token={linkToken}
          getDetails={(data) => setDetails(data)}
          params={paramsChanged}
        />
      ) : (
        renderGetData()
      )}
    </div>
  );
};

export default Home;
