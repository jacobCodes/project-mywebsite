import { IExtensionResponse } from 'q2-tecton-sdk/dist/esm/sources/requestExtensionData';
import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ofType, StateObservable } from 'redux-observable';
import { Dependencies } from '~/store';
import {
  AccountDetailsState,
  AccessTokenState,
  AccessTokenType
} from '../reducer/accessToken';

export const fetchAccessTokenEpic = (
  action$: Observable<LinkTokenAction>,
  _payload: StateObservable<LinkTokenState>,
  { tecton }: Dependencies
) =>
  action$.pipe(
    ofType(AccessTokenType.Fetch),
    mergeMap((_action: LinkTokenAction) => {
      return tecton.sources
        .requestExtensionData$<any>({
          route: 'get_account_details',
          body: { token: _action.payload.token, data: _action.payload.data }
        })
        .pipe(
          map((res: IExtensionResponse<any>) => {
            console.log('payload', res.data.account_details);
            return {
              type: AccessTokenType.FetchFinalized,

              payload: {
                accessToken: res.data.access_token.access_token,
                accountDetails: res.data.account_details
              }
            };
          }),
          catchError((err) =>
            of({
              type: AccessTokenType.FetchError,
              payload: { message: err.message, status: err.status }
            })
          )
        );
    })
  );

export default fetchAccessTokenEpic;
