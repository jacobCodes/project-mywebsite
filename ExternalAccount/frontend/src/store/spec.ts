import { connect } from 'q2-tecton-mock';
import { createQ2TectonObservable } from 'q2-tecton-observable';
import { createStore } from '.';

describe('createStore()', () => {
  it('should create store', async () => {
    const tecton = createQ2TectonObservable(await connect());
    const store = createStore(
      { tecton },
      { deleteMe: null },
      { deleteMe: () => null }
    )!;
    expect(typeof store.dispatch).toBe('function');
    expect(typeof store.getState).toBe('function');
    expect(typeof store.replaceReducer).toBe('function');
    expect(typeof store.subscribe).toBe('function');
  });
});
