import { AccountDetails } from '~/types';
export interface State {
  linkToken: String;
  data: AccountDetails;
}

const initialState: State = {
  linkToken: '',
  data: {}
};

export default initialState;
