import { fetchLinkTokenEpic } from '~/modules/link/epics';
import { fetchAccessTokenEpic } from '~/modules/accessToken/epics';

export default [fetchLinkTokenEpic, fetchAccessTokenEpic];
