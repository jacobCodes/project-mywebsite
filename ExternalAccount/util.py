import importlib
import os

def import_module(module_name, relative_path):
    full_path = os.path.abspath(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)), relative_path
        )
    )
    spec = importlib.util.spec_from_file_location(module_name, full_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module
