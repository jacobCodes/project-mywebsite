from injector import inject
from q2_sdk.core.exceptions import TectonError
from bindings import Context
from services import Plaid,Q2
from munch import munchify
import json
from services import Database

@inject
class Routes:
    def __init__(self, context: Context, plaid: Plaid,q2 : Q2, database: Database):
        self.context = context
        self.plaid = plaid
        self.database = database
        self.q2 = q2

    async def default(self):
        return {
            'context_id': id(self.context.request),
        }
        
    async def post(self):
        return {"data":"hello world"}
    
    async def get(self):
        return {"data":"get method"} 

    async def user_validation(self):
        # print("hello")
        acces_token=False
        try:
            access_token_in_db = await self.database.get('plaid_access_token')
            if access_token_in_db:
                print("access_token",access_token_in_db)
                acces_token=True

            else:
                access_token=False
            return {"acces_token":acces_token}

        except Exception as e:
            print("e",e)
            return e

    async def get_link_token(self):
        res = await self.plaid.get_link_token()
        return res

    async def get_access_token(self):
        res = await self.plaid.get_access_token(public_token)
        return res

    async def get_balance_info(self):
        account_id=None
        access_token = None
        try:
            account_id= await self.database.get("plid_account_id")
            access_token = await self.database.get("plid_access_token")
            if access_token and account_id:
                print(access_token ,"acces_token in get_balance_info")
                res = self.plaid.get_balance_info(access_token,account_id)
                print(res ,"get_balance_info")
                return {"data":res}
        except Exception as e:
            print(str(e)+ " error")
            return e

    async def get_identity_data(self):
        account_id = None
        access_token = None
        try:
            account_id= await self.database.get("plid_account_id")
            access_token = await self.database.get("plid_access_token")
            if access_token:
                print(access_token,"access_token in identity")
                res = self.plaid.get_identity_data(access_token)
                print(res,"get_identity_data")
                return {"data":res}
        except Exception as e:
            print(str(e)+ "error")
            return e

        access_token = await self.database.get("plaid_access_token")
        account_id = await self.database.get("plaid_account_id")
        print(access_token ,"acces_token in get_identity_data")
        res = self.plaid.get_identity_data(access_token)
        print(res ,"get_identity_data")
        return {"data":res}

    async def get_account_details(self):
        account_info = munchify(self.context.form_fields)
        account_id = str(account_info.data)
        await self.database.set("plaid_account_id",account_id,user_id=self.context.online_user.user_id)
        account_id_from_db = await self.database.get("plaid_account_id")
        print(account_id_from_db,"account_id")
        # save account_id
        public_token = str(account_info.token)
        await self.database.set("plaid_public_token",public_token,user_id=self.context.online_user.user_id)
        # await self.database.set("plaid_public_token",public_token ,user_id=self.context.online_user.user_id)
        # print(public_token,"public_token")
        # access_token=None
        # try:
        #     access_token_in_db = await self.database.get('plaid_access_token')
        #     if access_token_in_db:
        #         access_token=access_token_in_db
        #     else:
        #         print('hello')
        #         # access_token = await self.plaid.get_access_token(public_token)
        #         # print(access_token,"really access_token")

        # except Exception as e:
        #     return e
        access_token = await self.plaid.get_access_token(public_token)
        # access_token = await self.database.get("plaid_access_token")
        print(access_token,"access_token-+++++++++++++++++++++++++++++++")
        oauth_info = await self.plaid.get_auth_info(
            access_token["access_token"],account_id_from_db
        )

        print(oauth_info,"oauth_info")


        balance_info =  self.plaid.get_balance_info(access_token["access_token"],account_id_from_db)

        identity_data =  self.plaid.get_identity_data(access_token["access_token"],account_id_from_db)

        # identity = await self.plaid.get_identity_data(access_token)
        # print(identity,"identity info ")
        # ach_values = oauth_info["numbers"]["ach"]
        # bank_reqs = oauth_info["accounts"]
        # for bank_type_detail in bank_reqs:
        #     bank_type = bank_type_detail["type"]
        # for ach_data in ach_values:
        #     account_number = ach_data["account"]
        #     account_routing = ach_data["routing"]
        # external_account_response = await self.q2.add_external_account(bank_type,account_number,account_routing)
        # print(external_account_response,"external_account_response")

        return {'access_token': access_token, 'account_details': oauth_info}
        # access_token = None
        # try:
        #     access_token = await self.database.get("access_token")
        #     print(access_token,"from db ======================================sadsad==================")
        #     if access_token:
        #         print("hello access token")
        #         oauth =  await self.plaid.get_auth_info(
        #             access_token["access_token"], str(account_info.data)
        #         )

        #         print(oauth,"oauth_info")
        #         print("in if block in try")

        #         # print(str(account_info.data),"response.data so called ===================================")

        #         # print(oauth_info,"oauth_info")
        #         # return {'access_token': access_token, 'account_details': oauth_info}
        #     else:
        #         access_token = await self.plaid.get_access_token(public_token)
        #         oauth_info =  await self.plaid.get_auth_info(
        #             access_token["access_token"], str(account_info.data)
        #         )

        #         print("else statement")

        #         print(str(account_info.data),"response.data so called")

        #         print(oauth_info,"oauth_info")
        #         return {'access_token': access_token, 'account_details': oauth_info}
        # except Exception as e:
        #     print("error message", e)


    async def test_external(self):
        print('test_external')
        return {
            'hello':'world'
        }
