from q2_sdk import core
from q2_sdk.hq.db.user_data import UserData
from q2_sdk.core.http_handlers.tecton_client_handler import Q2TectonClientRequestHandler
from injector import inject
from bindings import Context, Logger
from request_scope import request

ExecuteStoredProcedure = core.dynamic_imports.api_ExecuteStoredProcedure
DataType = ExecuteStoredProcedure.DataType

@inject
class Database:
    def __init__(self, logger: Logger, context: Context):
        self.context = context
        self.logger = logger

    # async def call_stored_procedure(
    #     self, db_object, stored_procedure, kwargs, args
    # ):

    #     sql_params = []
    #     for arg_name, arg_value in kwargs.items():
    #         arg_type = args[arg_name]
    #         sql_params.append(
    #             ExecuteStoredProcedure.SqlParam(
    #                 arg_type, arg_name, str(arg_value)
    #             )
    #         )
    #     results = await db_object.call_hq(
    #         stored_procedure, ExecuteStoredProcedure.SqlParameters(sql_params)
    #     )
    #     payload = []
    #     for result in results:
    #         items = {}
    #         for key in dir(result):
    #             item = getattr(result, key)
    #             if hasattr(item, 'pyval'):
    #                 items[key[0].lower() + key[1:]] = item.pyval
    #         payload.append(items)
    #     return payload

    async def set(self, key, value, user_id=None):
        user_data = UserData(self.logger)
        if not user_id:
            if not hasattr(self.context, 'online_user'):
                return None
            user_id = self.context.online_user.user_id
        await user_data.create(user_id, key, value)
        return value

    async def get(self, key, user_id=None):
        user_data = UserData(self.logger)
        if not user_id:
            if not hasattr(self.context, 'online_user'):
                return None
            user_id = self.context.online_user.user_id

        results = await user_data.get(user_id, key)
        return str(results[0]['GTDataValue']) if len(results) else None

    # async def delete(self, key):
    #     user_data = UserData(self.context.logger)
    #     if not hasattr(self.context, 'online_user'):
    #         return None
    #     user_id = self.context.online_user.user_id
    #     results = await user_data.get(user_id, key)
    #     if len(results):
    #         await user_data.delete(
    #             user_id, results[0]['DataID'], self.context.online_session,
    #             self.context.online_user
    #         )
    #     return None
