from q2_sdk.core.exceptions import TectonError

class Error(object):
    class BaseError(TectonError):
        def __init__(self, message, status=400, payload=None, meta=None):
            if payload is None:
                payload = {}
            if meta is None:
                meta = {}
            data = {'meta': meta, 'payload': payload, 'status': status}
            super().__init__(message, data)
