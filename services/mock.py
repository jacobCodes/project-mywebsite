import glob
import os
import re
import yaml

requests = None
routes = None

class MockRequest:
    text = ''
    headers = {}

    def __init__(self, headers=None, text=''):
        if headers is None:
            headers = {}
        self.headers = headers
        self.text = text

class Mock:
    @property
    def requests(self):
        global requests
        if not requests:
            requests = self.load('requests')
        return requests

    @property
    def routes(self):
        global routes
        if not routes:
            routes = self.load('routes')
        return routes

    def load(self, schema):
        mocks = []
        paths = glob.glob(
            os.path.abspath(
                os.path.
                join(os.path.dirname(os.path.realpath(__file__)), '../mocks')
            ) + '/' + schema + '/*.yaml'
        )
        for path in paths:
            with open(path, 'r') as f:
                data = yaml.safe_load(f)
                if isinstance(data, list):
                    mocks = mocks + data
                else:
                    mocks.append(data)
        return mocks

    def request(self, url, method='GET', headers=None, data=None):
        if headers is None:
            headers = {}
        response = None
        for request in self.requests:
            if 'request' not in request or 'response' not in request:
                continue
            if (
                'url' not in request['request'] or request['request']['url']
                == url or re.match(request['request']['url'], url)
            ) and (
                'method' not in request['request'] or
                request['request']['method'] == method
            ) and (
                'headers' not in request['request'] or
                self.headers_match(request['request']['headers'], headers)
            ):
                response = request['response']
                break
        if response:
            return MockRequest(
                response['headers'] if 'headers' in response else {},
                response['body'] if 'body' in response else ''
            )
        return None

    def normalize_keys(self, dictionary, normalizer, depth=0):
        if isinstance(dictionary, list):
            items = []
            for item in dictionary:
                items.push(self.normalize_key(item, normalizer, depth))
            return items
        if isinstance(dictionary, dict):
            result = {}
            for key, value in dictionary.items():
                if depth and (
                    isinstance(value, dict) or isinstance(value, list)
                ):
                    value = self.normalize_keys(value, normalizer, depth - 1)
                result[normalizer(key)] = value
            return result
        return dictionary

    def headers_match(self, headers_a=None, headers_b=None):
        headers_a = self.normalize_keys(headers_b, lambda x: x.upper())
        headers_b = self.normalize_keys(headers_b, lambda x: x.upper())
        for key, value in headers_a.items():
            if (value is None and key in headers_b
                ) or (key in headers_b and headers_b[key] == value):
                continue
            return False
        return True
