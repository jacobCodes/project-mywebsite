from injector import inject
from plaid import Client
from bindings import Logger, Context
from .database import Database
from .secrets import Secrets
from .response import Response

@inject
class Plaid:
    def __init__(self, db: Database, context: Context, secrets: Secrets,response:Response):
        self.db = db
        self.context = context
        self.secrets = secrets
        self.response = response


    def create_client_instance(self):
        client_id = self.secrets.get_client_id()
        client_secret = self.secrets.get_client_secret()
        client = Client(
            client_id=client_id, secret=client_secret, environment='sandbox'
        )
        return client

    async def get_link_token(self):
        client = self.create_client_instance()
        configs = {
            'user': {
                'client_user_id': '1234-test-user-id',
            },
            'products': ['auth', 'transactions'],
            'client_name': 'Plaid First Test App',
            'country_codes': ['US'],
            'language': 'en',
            'webhook': 'https://sample-webhook-uri.com',
            'link_customization_name': 'default',
            'account_filters': {
                'depository': {
                    'account_subtypes': ['checking', 'savings'],
                },
            },
        }
        response = client.LinkToken.create(configs)
        link_token = response['link_token']
        await self.db.set("plaid_link_token",link_token,user_id=self.context.online_user.user_id)
        return {'data':link_token}
    
    
    async def get_access_token(self, public_token):
        client = self.create_client_instance()
        response = client.Item.public_token.exchange(public_token)
        access_token = response['access_token']
        await self.db.set("plaid_access_token",access_token, user_id=self.context.online_user.user_id)
        data = {'access_token': access_token}
        return data


    async def get_auth_info(self, access_token,account_id):
        access_token_in_db = None
        try:
            access_token_in_db = await self.db.get("plaid_access_token")
            print(access_token_in_db,"access_token in db")
            if access_token_in_db:
                # account_id = await self.db.get("plaid_account_id")
                client = self.create_client_instance()
                response = client.Auth.get(access_token,{"account_ids":[account_id]})
                return response
            else:
                client = self.create_client_instance()
                # access_token = await self.get_access_token(public_token)
                response = client.Auth.get(access_token,{"account_ids":[account_id]})
                return response
        except Exception as e:
            print(str(e) + "errror")
            return e
    
    
    def get_balance_info(self,access_token,account_id):
        client = self.create_client_instance()
        response = client.Accounts.balance.get(access_token,{"account_ids":[account_id]})
        accounts = response['accounts']
        print(response,"response of balance")
        return accounts
    
    
    def get_identity_data(self,access_token,account_id):
        client = self.create_client_instance()
        options = {"account_ids":[account_id]}
        print(dir(client.Identity))
        response = client.Identity.get(access_token)
        # response = client.Identity.get(access_token)
        accounts = response['accounts']
        # print(accounts ,"accoutns")
        # print(response,"response of identity")
        for account in accounts:
            identities = account['owners']
        print(identities,"identities owners")
        return identities
        
    
    # async def get_blance_info(self,access_token):
        # client = self.create_client_instance()
        # response = client.Accounts.balance.get(access_token)
        # balance = response["accounts"]
        # print(response,"fghgjhkj")
        # print(balance,"fhgjhj")
        # access_token = None
        # try:
        #     access_tokon_in_db = await self.db.get("plaid_access_token")
        #     print(access_tokon_in_db)
        #     if access_tokon_in_db:
        #         client = self.create_client_instance()
        #         response = client.Accounts.balance.get(access_token)
        #         balance = response["accounts"]
        #         print(response)
                
        #         return balance
            
        #     else:
        #         client = self.create_client_instance()
        #         # access_token = await self.get_access_token(public_token)
        #         response = client.Accounts.balance.get(access_token)
        #         balance = response["accounts"]
        #         print(response)
        #         return balance
        # except Exception as e:
        #     return e 
        
        
    # async def get_identity_data(self,access_token):
        # access_token = None
        # try:
        #     access_tokon_in_db = await self.db.get("plaid_access_token")
        #     print(access_tokon_in_db)
        #     if access_tokon_in_db:
        #         client = self.create_client_instance()
        #         response = client.Identity.get(access_token)
        #         accounts = response['accounts']
        #         return accounts
        #     else:
        #         client = self.create_client_instance()
        #         # access_token = await self.get_access_token(public_token)
        #         response = client.Identity.get(access_token)
        #         accounts = response['accounts']
        #         return accounts
        # except Exception as e:
        #     return e      
