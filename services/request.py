from q2_sdk.core import q2_requests
from injector import inject
from bindings import Logger
from request_scope import request
from .meta import Meta

@inject
@request
class Request:
    def __init__(self, meta: Meta, logger: Logger):
        self.meta = meta
        self.logger = logger
        self.mock = False

    async def get(
        self,
        url,
        params=None,
        session=None,
        headers=None,
        mock=None,
        **kwargs
    ):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'GET', headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(url, 'GET', headers=headers, mock=mock)
        if not response:
            response = await q2_requests.get(
                self.logger,
                url,
                params=params,
                session=session,
                headers=headers,
                **kwargs
            )
        track_response(response)
        return response

    async def options(
        self, url, session=None, headers=None, mock=None, **kwargs
    ):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'OPTIONS', headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(
            url, 'OPTIONS', headers=headers, mock=mock
        )
        if not response:
            response = await q2_requests.options(
                self.logger, url, session=session, headers=headers, **kwargs
            )
        track_response(response)
        return response

    async def post(
        self,
        url,
        data=None,
        json=None,
        session=None,
        headers=None,
        mock=None,
        **kwargs
    ):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'POST', data=data, headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(
            url, 'POST', data=data, headers=headers, mock=mock
        )
        if not response:
            response = await q2_requests.post(
                self.logger,
                url,
                data=data,
                json=json,
                session=session,
                headers=headers,
                **kwargs
            )
        track_response(response)
        return response

    async def head(self, url, session=None, headers=None, mock=None, **kwargs):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'HEAD', headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(url, 'HEAD', headers=headers, mock=mock)
        if not response:
            response = await q2_requests.head(
                self.logger, url, session=session, headers=headers, **kwargs
            )
        track_response(response)
        return response

    async def put(
        self, url, data=None, session=None, headers=None, mock=None, **kwargs
    ):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'PUT', data=data, headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(
            url, 'PUT', data=data, headers=headers, mock=mock
        )
        if not response:
            response = await q2_requests.put(
                self.logger,
                url,
                data=data,
                session=session,
                headers=headers,
                **kwargs
            )
        track_response(response)
        return response

    async def patch(
        self, url, data=None, session=None, headers=None, mock=None, **kwargs
    ):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'PATCH', data=data, headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(
            url, 'PATCH', data=data, headers=headers, mock=mock
        )
        if not response:
            response = await q2_requests.patch(
                self.logger,
                url,
                data=data,
                session=session,
                headers=headers,
                **kwargs
            )
        track_response(response)
        return response

    async def delete(
        self, url, session=None, headers=None, mock=None, **kwargs
    ):
        headers = self.filter_headers(headers)
        track_response = self.track_request(
            url, 'DELETE', headers=headers, mock=mock, **kwargs
        )
        response = self.mock_request(url, 'DELETE', headers=headers, mock=mock)
        if not response:
            response = await q2_requests.delete(
                self.logger, url, session=session, headers=headers, **kwargs
            )
        track_response(response)
        return response

    def filter_headers(self, headers=None):
        if not headers:
            return None
        filtered_headers = {}
        if headers:
            for key, value in list(headers.items()):
                if value is not None:
                    filtered_headers[key] = value
        return filtered_headers

    def mock_request(self, url, method, data=None, headers=None, mock=None):
        if not self.mock and not mock:
            return None
        return self.mock.request(
            url, data=data, method=method, headers=headers
        )

    def track_request(
        self, url, method, data=None, headers=None, mock=False, **kwargs
    ):
        req = {'method': method.upper(), 'mock': mock, 'url': url, **kwargs}
        if headers:
            req['headers'] = headers
        if data:
            req['body'] = data
        self.meta.requests.append(request)

        def track_response(res):
            req['response'] = {'body': res.text, 'headers': dict(res.headers)}

        return track_response
