import json
import os
from plaid import Client
from injector import inject
from bindings import Logger, Context
from .database import Database
from .meta import Meta

@inject
class Secrets:
    def __init__(self, db: Database, context: Context):
        self.db = db
        self.context = context

    def get_client_id(self):
        client_id = None
        try:
            client_id = os.environ['CLIENT_ID']
        except Exception as e:
            pass
        if not client_id:
            raise Exception('client id not found in env')
        return client_id

    def get_client_secret(self):
        client_secret = None
        try:
            client_secret = os.environ['CLIENT_SECRET']
        except Exception as e:
            pass
        if not client_secret:
            raise Exception('client secret not found in env')
        return client_secret
