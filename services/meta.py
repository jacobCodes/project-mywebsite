from request_scope import request

@request
class Meta:
    def __init__(self):
        self.requests = []
        self.tracebacks = []
        self.meta = {}

    def set(self, key, value):
        self.meta[key] = value

    @property
    def dict(self):
        return {
            'requests': self.requests,
            'tracebacks': self.tracebacks,
            **self.meta
        }
