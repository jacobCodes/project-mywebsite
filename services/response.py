import traceback

class Response(object):
    def success(self, payload=None, status=200, normalize=False):
        if payload is None:
            payload = {}
        s = self.services
        if normalize:
            normalizer = s.util.camel
            if callable(normalize):
                normalizer = normalize
            payload = s.util.normalize_keys(payload, normalizer)
        return {'meta': s.meta.dict, 'payload': payload, 'status': status}

    def error(self, e):
        s = self.services
        try:
            if isinstance(e, s.error.BaseError):
                e.args[1]['meta'] = s.meta.dict
                raise e
            message = str(e)
            traceback.print_exc()
            s.meta.tracebacks.append(traceback.format_exc())
            raise s.error.BaseError(message, 500, {}, s.meta.dict)
        except Exception:
            traceback.print_exc()
            raise s.error.BaseError(
                message, 500, {}, {'tracebacks': [traceback.format_exc()]}
            )
