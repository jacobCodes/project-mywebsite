import csv
import io
import json
import os
import re
from datetime import datetime, date
from q2_sdk.core import q2_requests

pascal2snake_pattern = re.compile(r'(?<!^)(?=[A-Z])')

class DeployEnv:
    deploy_env = os.environ['DEPLOY_ENV'].lower(
    ) if 'DEPLOY_ENV' in os.environ else 'stg'

    #  detect using correct environment variable
    @property
    def dev(self):
        return os.environ[
            'HQ_URL'
        ] == 'http://dev-01.q2developer.com/sdk/sfcu2/hq' if 'HQ_URL' in os.environ else False

    @property
    def prod(self):
        return self.deploy_env == 'prod'

    @property
    def stg(self):
        return self.deploy_env == 'stg'

class Util:
    deploy_env = DeployEnv()

    async def request(
        self, url, method='GET', headers=None, data=None, mock=None, **kwargs
    ):
        s = self.services
        r = None
        new_headers = {}
        if headers:
            # for key, value in headers.items():
            for key, value in list(headers.items()):
                if value is not None:
                    new_headers[key] = value
        headers = new_headers
        request = {
            'body': data,
            # 'headers': headers,
            'method': method.upper(),
            'mock': True,
            'url': url,
        }
        if mock is True or (mock is None and s.util.deploy_env.dev):
            r = s.mock.request(url, method=method, headers=headers)
        if r is None:
            request['mock'] = False
            r = await getattr(q2_requests, method.lower())(
                self.logger, url, headers=headers, data=data, **kwargs
            )
        request['response'] = {'body': r.text, 'headers': dict(r.headers)}
        if self.context.wedge_address_configs['q2_debugger']:
            s.meta.requests.append(request)
        return r

    def smart_parse(self, data: str):
        try:
            return json.loads(data)
        except Exception as e:
            if isinstance(e, json.decoder.JSONDecodeError):
                return data
            raise e

    def normalize_keys(self, dictionary, normalizer, depth=0):
        if isinstance(dictionary, list):
            items = []
            for item in dictionary:
                items.push(self.normalize_key(item, normalizer, depth))
            return items
        if isinstance(dictionary, dict):
            result = {}
            for key, value in dictionary.items():
                if depth and (
                    isinstance(value, dict) or isinstance(value, list)
                ):
                    value = self.normalize_keys(value, normalizer, depth - 1)
                result[normalizer(key)] = value
            return result
        return dictionary

    def pascal2snake(self, string):
        return pascal2snake_pattern.sub('_', string).lower()

    def snake2pascal(self, string):
        return ''.join(word.title() for word in string.split('_'))

    def snake2camel(self, string):
        string = self.snake2pascal(string)
        return string[0].lower() + string[1:]

    def camel2snake(self, string):
        return self.pascal2snake(string[0].upper() + string[1:])

    def camel(self, string):
        return self.snake2camel(self.camel2snake(string))

    def get_value(self, dictionary, key, default=None):
        return dictionary[key] if key in dictionary else default

    def obj2dict(self, obj, depth=0):
        result = {}
        if isinstance(obj, list):
            items = []
            for item in obj:
                items.push(self.obj2dict(item, depth))
            return items
        for key in dir(obj):
            if len(key) and key[0] == '_':
                continue
            value = getattr(obj, key)
            if isinstance(value, int) or isinstance(
                value, float
            ) or isinstance(value, str) or isinstance(value, bool):
                result[key] = value
            elif depth and (
                isinstance(value, object) or isinstance(value, list)
            ):
                result[key] = self.obj2dict(value, depth - 1)
        return result

    def csv(self, columns, data):
        output = io.StringIO()
        rows = []
        writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
        for item in data:
            row = []
            for column in columns:
                row.append(str(item[column]) if column in item else None)
            rows.append(row)
        writer.writerow(columns)
        for row in rows:
            writer.writerow(row)
        return output.getvalue()

    async def get_age(self, dob):
        s = self.services
        if not dob:
            raise s.error.BaseError('dob cannot be empty or none')
        born_date = datetime.strptime(dob, '%m-%d-%Y')
        current_data = date.today()
        age = current_data.year - born_date.year - (
            (current_data.month, current_data.day) <
            (born_date.month, born_date.day)
        )
        return age

    def format_ssn(self, ssn):
        formatted_ssn = ssn[0:3] + \
            '-' + ssn[3:5] + '-' + ssn[5:9]
        return formatted_ssn

    async def format_phone_number(self, phone):
        formatted_phone_number = phone[0:3] + \
            '-' + phone[3:6] + '-' + phone[6:10]
        return formatted_phone_number

    def format_core_address(self, address):
        address = {
            'address1': address.address_1,
            'address2': address.address_2,
            'city': address.city,
            'state': address.state,
            'zipcode': address.zipcode,
            'address_type': address.address_type,
            'country': address.country,
            'province': address.province,
            'additional_details': address.additional_details
        }
        return address

    def format_core_phone(self, phone):
        if not phone:
            return None
        phone = '{}{}{}'.format(
            phone.area_code, phone.phone_number[:3], phone.phone_number[3:]
        )
        return phone
