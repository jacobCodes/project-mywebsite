# from q2_sdk.hq.hq_api.wedge_online_banking import AddExternalAccount,ExecuteWedgeRequestByName
from q2_sdk.hq.hq_api.wedge_online_banking import AddExternalAccount
from q2_sdk.hq.hq_api.q2_api import ExecuteWedgeRequestByName
from bindings import Context, Logger
from injector import inject
from q2_sdk.core import q2_requests



@inject
class Q2:
    def __init__(self,logger:Logger,context:Context):
        self.logger = logger
        self.context = context

    async def add_external_account(self,account_type,account_number,account_aba, **kwargs):
        try:
            params_obj = AddExternalAccount.ParamsObj(logger=self.logger,hq_credentials=self.context.hq_credentials,allow_withdrawal=True,allow_deposit=True,account_aba=account_aba,account_number=account_number,account_type=account_type)
            response = await AddExternalAccount.execute(params_obj,use_json=True, **kwargs)
            return response
        except Exception as e:
            return e

    async def get_request_extension_data(self,request_options={"route":"get_balance_info","body":{}},extension=None,url=None):
        print("in request data")
        # url = url
        # request_data={"formData":"routing_key=get_link_token&data=e30%3D"}
        if url is not None:
            print("url",url)
            # Api call
                # request url (method 2)
                # response = q2_requests.post(self.logger,"https://sdk-shared-dev.q2devstack.com/2419/ExternalAccount",params = {"formData":"routing_key=get_link_token&data=e30%3D"})
            response = await q2_requests.post(self.logger,str(url),data={"formData":"routing_key=get_link_token&data=e30%3D"})
            return {"data":response}
            # resquest extension code (method 1)
        print("after if")
        # SDK Method
        params_obj = ExecuteWedgeRequestByName.ParamsObj(logger=self.logger,wedge_address_short_name="test_external",wedge_request_data=None,hq_credentials=self.context.hq_credentials)
        response = await ExecuteWedgeRequestByName.execute(params_obj,use_json=True)
        print(response.json())
        # return response
        return {"test":"ksf"}



