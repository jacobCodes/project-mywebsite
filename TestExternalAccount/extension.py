"""
TestExternalAccount Extension
"""

from q2_sdk.core.http_handlers.ardent_handler import Q2ArdentRequestHandler
# from q2_sdk.hq.models.db_config.db_config import DbConfig
# from q2_sdk.hq.models.db_config.db_config_list import DbConfigList
from .install.db_plan import DbPlan

class TestExternalAccountHandler(Q2ArdentRequestHandler):

    ## REQUIRED_CONFIGURATIONS is a dictionary of key value pairs that are necessary
    ## for the extension to run. If set, ensures the entries are set in the
    ## extension's settings file or the web server will not start.
    ## Keys are names and values are defaults written into the settings file.
    ## To override the defaults, generate the config (`q2 generate_config`) and
    ## then alter the resulting file

    # REQUIRED_CONFIGURATIONS = {
    #    # 'CONFIG1': 'Default',
    #    # 'CONFIG2': 'Default',
    # }

    # # Behaves the same way as REQUIRED_CONFIGURATIONS, but will not stop the web server
    # # from starting if omitted from the extension's settings file
    # OPTIONAL_CONFIGURATIONS = {}

    # # Behaves in a similar manner to REQUIRED_CONFIGURATIONS,
    # # but stores the data in the database instead of the settings file. Will be
    # # written into the database on `q2 install`
    # WEDGE_ADDRESS_CONFIGS = DbConfigList(
    #     [
    #         DbConfig('YourKeyHere', 'DefaultValue'),
    #     ]
    # )

    # Set this to True if you want to change which Core is configured based on database configuration
    DYNAMIC_CORE_SELECTION = False

    # This value will be used as the database 'description' row
    DESCRIPTION = ''

    DB_PLAN = DbPlan()

    CONFIG_FILE_NAME = 'TestExternalAccount' # configuration/TestExternalAccount.py file must exist if REQUIRED_CONFIGURATIONS exist

    def __init__(self, application, request, **kwargs):
        """
        If you need variables visible through the lifetime of this request,
        feel free to add them in this function
        """
        super().__init__(application, request, **kwargs)
        self.allow_non_q2_traffic = True

        # self.variable_example = 12345

    # # Uncomment this to allow PyCharm to give you better hinting on a specific core (Symitar in this example)
    # from q2_cores.Symitar.core import Core as SymitarCore
    # @property
    # def core(self) -> SymitarCore:  # pylint: disable=undefined-variable

    #     # noinspection PyTypeChecker
    #     return super().core

    @property
    def router(self):
        """
        Your extension's routing map. To handle a request, a method must be listed here. When a POST request is
        made to this extension with a routing_key value, the extension will route the request to the method linked
        to that key. The methods referenced here are defined below.
        """
        router = super().router
        router.update({
            'default': self.default,
            'submit': self.submit,
            # Add new routes here
        })

        return router

    def get(self):
        """
        Most Q2 extensions will be handling POSTs from the Online component, but
        GET requests can also be handled here. If you delete this function, GET
        will simply not be a supported Verb for this extension.
        PUT, DELETE, etc can also be handled by creating an appropriately named
        function. This is based off of Tornado's request handling.
        More info at http://www.tornadoweb.org/en/stable/guide/structure.html?highlight=post#subclassing-requesthandler
        """
        self.write("Hello World GET: From TestExternalAccount")
        return {"data":"smthng but wrold"}
    
    
    def post(self):
        print("hello post metho")
        return {"data":"data from post"}

    async def default(self):
        """
        This is the default route, which will handle any POST requests submitted without
        a routing key.

        All methods of this request handler have access to the following properties:

        - self.form_fields - Python dictionary of any input fields from the
            HTML user interface calling this extension
        """
        # If you have wedge address configs, you will need to use the following method
        # await self.get_wedge_address_configs()
        # Then you would access the configs throught the self.wedge_address_configs variable
        template = self.get_template('index.html.jinja2', {})
        return template

    async def submit(self):
        """
        This route will be called when your form is submitted, as configured above.
        """
        # If you have wedge address configs, you will need to use the following method
        # await self.get_wedge_address_configs()
        # Then you would access the configs throught the self.wedge_address_configs variable
        template = self.get_template(
            'submit.html.jinja2', {
                'header': "TestExternalAccount",
                'message': 'Hello World POST: From "TestExternalAccount".<br>',
                'data': self.form_fields
            }
        )

        return template
