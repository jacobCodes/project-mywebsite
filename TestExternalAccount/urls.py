from tornado.web import url
from .extension import TestExternalAccountHandler

# url('/regex_pattern', HandlerClass, kwargs=None, name=None)
# http://www.tornadoweb.org/en/stable/web.html?#tornado.web.URLSpec

URL_PATTERNS = [
    url(
        r'/TestExternalAccount/?(?P<routing_key>[\w.-]+)?',
        TestExternalAccountHandler
    ),
]
