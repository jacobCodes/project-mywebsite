You can add assets and images to this folder and to access them, you can call "self.base_assets_url/filename_of_your_asset". 
If referencing it from a jinja template, you can use "{{this.base_assets_url}}" instead.
