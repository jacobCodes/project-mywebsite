You can add any assets and images you want to use in the frontend to the public directory.

The SDK does not serve up the images located in public by accessing them directly.
At startup, using the build command in the package.json file included in this directory, we populate a dist directory 
with the contents of the public directory.

You can then use the SDK to reference the images located there with self.base_assets_url which resolves to dist

If using the default package.json, you can access your assets by calling "self.base_assets_url/filename_of_your_asset"
You would call "{{this.base_assets_url}}" if you are referencing your assets from a jinja template.



Feel free to change the build commands in package.json to whatever suits your needs.

The ultimate end goal is to get all your stuff into dist to serve up, and dist is not going to be checked into the 
git repo, so if you want it to show up when running in our datacenter, you’ll need an appropriate build command to do it for you.

