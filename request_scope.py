from injector import Scope, ScopeDecorator, InstanceProvider
import threading

class RequestScope(Scope):
    REQUEST_SCOPE_KEY = 'request_scope'

    def configure(self):
        self._locals = threading.local()

    def enter(self):
        assert not hasattr(self._locals, self.REQUEST_SCOPE_KEY)
        setattr(self._locals, self.REQUEST_SCOPE_KEY, {})

    def exit(self):
        scope = getattr(self._locals, self.REQUEST_SCOPE_KEY) if hasattr(
            self._locals, self.REQUEST_SCOPE_KEY
        ) else None
        if scope:
            for key, provider in scope.items():
                injectable = provider.get(self.injector)
                if hasattr(injectable, 'close'):
                    close = getattr(injectable, 'close')
                    if callable(close):
                        close()
                delattr(self._locals, repr(key))
            delattr(self._locals, self.REQUEST_SCOPE_KEY)

    def get(self, key, provider):
        try:
            return getattr(self._locals, repr(key))
        except AttributeError:
            provider = InstanceProvider(provider.get(self.injector))
            setattr(self._locals, repr(key), provider)
            try:
                scope = getattr(self._locals, self.REQUEST_SCOPE_KEY)
            except AttributeError:
                raise Exception(f'{key} does not exist in scope request')
            scope[key] = provider
            return provider

request = ScopeDecorator(RequestScope)
