{ nixpkgs ? import <nixpkgs> {} }:

nixpkgs.stdenv.mkDerivation rec {
  name = "accounts";
  buildInputs = [
    nixpkgs.gnumake42
    nixpkgs.gnused
    nixpkgs.python38
    nixpkgs.python38Packages.pip
    nixpkgs.python38Packages.virtualenv
  ];
}
